from django.apps import AppConfig


class ParsexmlConfig(AppConfig):
    name = 'parsexml'
