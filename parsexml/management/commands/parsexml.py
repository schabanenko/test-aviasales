import os
import math
import xml.etree.ElementTree as ET
import datetime

from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction

from flight.models import Route, Flight, Price


class Parser:

    def __init__(self, path, filename):
        self.filename = filename
        self.path = path
        self.tree = self.get_tree()

    def get_tree(self):
        tree = ET.parse(os.path.join(self.path, self.filename))
        return tree

    def update_prices(self, routexml, routeobj):
        pricing = routexml.find('./Pricing')
        for charge in pricing.findall('./ServiceCharges'):
            Price.objects.update_or_create(
                charge_type=charge.attrib.get('ChargeType', ''),
                person_type=charge.attrib.get('type', ''),
                currency=pricing.attrib.get('currency', ''),
                route=routeobj,
                defaults={'price': charge.text}
            )
        routeobj.weight = self.calculate_weight(routeobj)
        routeobj.save()

    @staticmethod
    def calculate_weight(route):
        price = route.prices.filter(
            person_type=settings.DEFAULT_PERSON_TYPE,
            charge_type=settings.DEFAULT_CHARGE_TYPE
        ).first()
        route_time = route.route_time
        weight = math.sqrt(
            (settings.PRICE_WEIGHT*price.price)**2
            + route_time**2)
        return weight

    def parse(self):
        root = self.tree.getroot()
        routes = root.findall('./PricedItineraries/Flights')
        for route in routes:
            onward = route.find('./OnwardPricedItinerary')
            fls = onward.find('./Flights')
            flytime = 0
            waittime = 0
            r, rcreated = Route.objects.get_or_create(source='', destination='')
            arrival = None
            departure = None
            source = None
            destination = None
            for fly in fls.findall('./Flight'):
                departure_time_stamp = fly.find('./DepartureTimeStamp').text
                departure_time_stamp = datetime.datetime.strptime(departure_time_stamp, "%Y-%m-%dT%H%M")

                arrival_time_stamp = fly.find('./ArrivalTimeStamp').text
                arrival_time_stamp = datetime.datetime.strptime(arrival_time_stamp, "%Y-%m-%dT%H%M")

                flytime += (arrival_time_stamp - departure_time_stamp).seconds
                fare_basis = fly.find('./FareBasis').text
                fare_basis = fare_basis.strip()
                obj, created = Flight.objects.get_or_create(
                    source=fly.find('./Source').text,
                    destination=fly.find('./Destination').text,
                    carrier=fly.find('./Carrier').text,
                    carrier_code=fly.find('./Carrier').attrib.get('id'),
                    flyclass=fly.find('./Class').text,
                    ticket_type=fly.find('./TicketType').text,
                    defaults={
                        'route': r,
                        'warning': fly.find('./WarningText').text,
                        'fare_basis': fare_basis,
                        'departure_time_stamp': departure_time_stamp,
                        'arrival_time_stamp': arrival_time_stamp,
                        'stops': fly.find('./NumberOfStops').text,
                    }
                )
                if not created:
                    obj.warning = fly.find('./WarningText').text
                    obj.fare_basis = fare_basis
                    obj.departure_time_stamp = departure_time_stamp
                    obj.arrival_time_stamp = arrival_time_stamp
                    obj.save()

                if departure is None:
                    departure = departure_time_stamp
                if arrival is None:
                    arrival = arrival_time_stamp

                if arrival != arrival_time_stamp:
                    waittime += (departure_time_stamp - arrival).seconds
                    arrival = arrival_time_stamp

                if source is None:
                    source = fly.find('./Source').text
                destination = fly.find('./Destination').text

            r.arrival_time_stamp = arrival
            r.departure_time_stamp = departure
            r.wait_time = waittime
            r.fly_time = flytime
            r.source = source
            r.destination = destination
            r.route_time = waittime + flytime
            r.filename = self.filename
            r.save()

            self.update_prices(route, r)


class Command(BaseCommand):
    help = 'The xml parser'

    def add_arguments(self, parser):
        parser.add_argument('--filename', type=str)

    @transaction.atomic
    def handle(self, *args, **options):
        path = settings.XML_PATH
        if options['filename']:
            xf = options['filename']
            parser = Parser(path, xf)
            parser.parse()

