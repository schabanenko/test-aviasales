from django.db import models
from simple_history.models import HistoricalRecords


# Create your models here.
class Route(models.Model):
    source = models.CharField('Source', max_length=255, blank=True)
    destination = models.CharField('Destination', max_length=255, blank=True)
    departure_time_stamp = models.DateTimeField('Departure time', blank=True, null=True)
    arrival_time_stamp = models.DateTimeField('Arrival time', blank=True, null=True)
    fly_time = models.BigIntegerField('Fly time', blank=True, null=True)
    wait_time = models.BigIntegerField('Wait time', default=0, blank=True, null=True)
    route_time = models.BigIntegerField('Route time', blank=True, null=True)
    weight = models.BigIntegerField('Weight', blank=True, null=True)
    filename = models.CharField('File name', max_length=255, blank=True, default='')
    history = HistoricalRecords()


class Flight(models.Model):
    source = models.CharField('Source', max_length=255)
    destination = models.CharField('Destination', max_length=255)
    departure_time_stamp = models.DateTimeField('Departure time')
    arrival_time_stamp = models.DateTimeField('Arrival time')
    carrier = models.CharField('Carrier', max_length=255)
    carrier_code = models.CharField('Carrier code', max_length=10)
    flyclass = models.CharField('Class', max_length=2)
    stops = models.PositiveSmallIntegerField('Number of stops')
    fare_basis = models.CharField('Fare Basis', max_length=255)
    warning = models.TextField('Warning text', blank=True, null=True)
    ticket_type = models.CharField('Ticket type', max_length=10)
    route = models.ForeignKey(Route, on_delete=models.CASCADE, related_name='flights')
    history = HistoricalRecords()


class Price(models.Model):
    charge_type = models.CharField('Charge type', max_length=255, blank=True)
    person_type = models.CharField('Type', max_length=255, blank=True)
    price = models.DecimalField(verbose_name='Price', max_digits=10, decimal_places=2, default=0)
    currency = models.CharField('Currency', max_length=4, blank=True)
    route = models.ForeignKey(Route, on_delete=models.CASCADE, related_name='prices')
    history = HistoricalRecords()

