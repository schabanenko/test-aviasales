from django.contrib import admin

from flight.models import Price, Route, Flight
# Register your models here.


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'source', 'destination',
        'departure_time_stamp', 'arrival_time_stamp',
        'fly_time', 'wait_time', 'route_time', 'weight'
    ]


@admin.register(Flight)
class FlightAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'source', 'destination',
        'departure_time_stamp', 'arrival_time_stamp',
        'carrier', 'carrier_code', 'flyclass',
        'stops', 'ticket_type'
    )


@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    list_display = ['id', 'price', 'route']
