from rest_framework import viewsets, filters
from django.db.models import Max

from flight.models import Route
from flight.serializers import RouteSerializer
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.


class RouteView(viewsets.ReadOnlyModelViewSet):
    """
    Выводит список маршрутов. Так-же, отдельно каждый маршрут.
    """
    queryset = Route.objects\
        .prefetch_related('prices', 'flights')\
        .annotate(mp=Max('prices__price')).filter()
    serializer_class = RouteSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['source', 'destination']
    ordering_fields = ['fly_time', 'wait_time', 'route_time', 'weight',
                       'departure_time_stamp', 'arrival_time_stamp', 'mp']
