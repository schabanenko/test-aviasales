from rest_framework import serializers

from flight.models import Route, Flight, Price


class HistorySerializer(serializers.ModelSerializer):
    def __init__(self, model, *args, fields='__all__', **kwargs):
        self.Meta.model = model
        self.Meta.fields = fields
        super().__init__()

    class Meta:
        pass


class FlightSerializer(serializers.ModelSerializer):

    history = serializers.SerializerMethodField()

    def get_history(self, obj):
        model = obj.history.__dict__['model']
        serializer = HistorySerializer(model, obj.history.all().order_by('history_date'), many=True)
        serializer.is_valid()
        return serializer.data

    class Meta:
        model = Flight
        fields = '__all__'


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = '__all__'


class RouteSerializer(serializers.ModelSerializer):
    flights = FlightSerializer(many=True, read_only=True)
    prices = PriceSerializer(many=True, read_only=True)
    history = serializers.SerializerMethodField()

    def get_history(self, obj):
        model = obj.history.__dict__['model']
        serializer = HistorySerializer(model, obj.history.all().order_by('history_date'), many=True)
        serializer.is_valid()
        return serializer.data

    class Meta:
        model = Route
        fields = '__all__'
